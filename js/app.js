var app = new Vue({
    el:"#app",
    data:{
        name:"surya",
        appName:"VueJS"
    }
});

var bind = new Vue({
    el:"#bind",
    data:{
        topic:"Data binding",
        link: "www.lowes.com",
        title:"This is dynamic title",
        fName:"Surya",
        lName:"Pratap",
    },
    methods:{
        getFullName: function(){
            return this.fName +" "+this.lName;
        }
    }
});

var event = new Vue({
    el: "#event",
    data: {
        topic: "event",
        counter: 0,
        show: false
    },
    methods:{
        add: function () {
            this.counter++;
        },
        abc:function(){
            alert("abc");
        },
        say: function (message) {
            this.abc();
            alert(message)
        }
    }
});

var eventModifier = new Vue({
    el: "#eventModifier",
    data: {
        topic: "Event Modifier",
    },
    methods: {
        stopProp: function () {
            console.log("stopped Propogation");
        },
        onSubmit: function () {
            console.log("form submitted");
        },
        stopPrevent: function () {
            console.log("form submitted");
        },
        doThis: function () {
            console.log("form submitted");
        },
        doThat: function () {
            console.log("form submitted");
        },

        warn: function (message, event) {
            // now we have access to the native event
            if (event) event.preventDefault()
            alert(message)
        }
    }
});

var keyModifier = new Vue({
    el: "#keyModifier",
    data: {
        topic: "Key Modifier",

    },
    methods: {
        submit: function(){
            alert("submitted");
        }
    }
});

var twoWayDataBinding = new Vue({
    el: "#twoWayDataBinding",
    data: {
        topic: "Two Way Data Binding",
        message:"",
        checked:true,
        checkedNames:[],
        picked:"",
        selected:"B"
    },
    methods: {
        
    }
});

var modifier = new Vue({
    el: "#modifier",
    data: {
        topic: "Modifier",
        message: "",
        number: "",
        trim: "",
    },
    methods: {

    }
});

var computedProperties = new Vue({
    el: "#computedProperties",
    data: {
        topic: "Computed Properties",
        age:20,
        a: 0,
        b: 0
    },
    methods: {
       reset:function(){
           this.a = 0;
           this.b = 0;
       }
       
    },
    computed:{
        addToA: function () {
            console.log("add to A");
            return this.age + this.a;
        },
        addToB: function () {
            console.log("add to B");
            return this.age + this.b;
        }
    }
});

var conditionals = new Vue({
    el: "#conditionals",
    data: {
        topic: "Conditionals",
        error: false,
        type:"A",
        show:false
        
    },
    methods: {
        set:function(val){
            this.type = val;
        }

    }
});

var listRendering = new Vue({
    el: "#listRendering",
    data: {
        topic: "List Rendering",
        pdList: [
            {
                name: "Samsung Washing Machine",
                price: "$200",
                show: false
            },
            {
                name: "LG Washing Machine",
                price: "$300",
                show: false
            },
            {
                name: "Heir Washing Machine",
                price: "$200",
                show: false
            },
            {
                name: "Wirlpool Washing Machine",
                price: "$900",
                show: false
            },
            {
                name: "Panasonic Washing Machine",
                price: "$400",
                show: false
            },
            {
                name: "Bosh Washing Machine",
                price: "$300",
                show: false
            }

        ]

    },
    methods: {

    }
});


var dynamicCSS = new Vue({
    el: "#dynamicCSS",
    data: {
        topic: "Dynamic CSS Classes",
        classes:{
            red:true,
            green:false
        }
    },
    methods: {
      
    }
});






